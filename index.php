<?php
    require_once('./includes/head.php');
?>
    <body class = "text-center">
        <div class = "container" id="containerHome">
            <form method = "GET" action = "verificacao.php">
                    <div class = "row">
                        <div class = "col-12 mb-5 text-center">
                            <img src="./includes/img/logo-branco.png">
                            <h1 class = "h2 mb-3 font-weight-normal">Carteirinha ABMCJ</h1>
                        </div>
                    </div>

                    <div class = "row justify-content-md-center">
                        <div class = "form-group col-md-4 mt-1">
                            <label for = "inputEmail" class = "form-label mb-3" style = "font-size: 29px">Informe o seu e-mail</label>
                            <input type = "email" class = "form-control" id = "inputEmail" name="email" placeholder = "seu@email.com.br" required></input>
               <!--              <a href = "index.php" style = "color: white"><small id="emailHelp" class="form-text text-muted mt-2"> Não possui um pedido em andamento? Clique aqui e preencha o formulário</small><a> -->
                        <div>
                    <div>

                    <div class = "form-group mt-5"> 
                        <button type = "submit" id="btnConsultar" class = "btn btn-info btn-block">Enviar</button>
                    </div> 
            </form>
        </div>        
    <div class="footer" id="footerHome">
      <a href="https://www.victorbraga.com.br" target="_blank" style="color: #888888!important;"><p id="footerTxt">Desenvolvido e Mantido por: Victor Braga Design e Desenvolvimento</p></a>
    </div>
    </body>
</html>
