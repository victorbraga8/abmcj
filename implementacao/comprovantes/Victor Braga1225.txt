Faturados Empresa

SELECT pc.nome as Empresa, pc.id as IdEmpresa, pc.emailNota as emailNFEmpresa, pc.cnpj, dpa.codigo, dp.nome, pc2.nome as Posto, pc2.id as IdPosto, pc2.emailNota as emailNFPosto, dpa.dataliberacaolaudo, pag.TipoPagamento FROM dadospessoais dp INNER JOIN dadospessoaisAmostra dpa ON dpa.iddadospessoais = dp.id INNER JOIN pagsegurotransacoes pag ON pag.id = dpa.pagamento_id INNER JOIN postocoleta pc ON pc.id = dpa.idpostocoleta INNER JOIN postocoleta pc2 ON pc2.id = pag.postocoleta_id WHERE pag.TipoPagamento = "Agendamento Pj" and dpa.dataliberacaolaudo >= '2021-04-01' and dpa.dataliberacaolaudo <= '2021-04-30'


Dados Bancários Empresa e Quantidade de Coleta

SELECT COUNT(dpa.id) as Qtd, pc.nome, b.nome, pc.agencia, pc.agenciaDv, pc.contaCorrente, pc.contaCorrenteDv FROM dadospessoaisAmostra dpa INNER JOIN postocoleta pc on dpa.idpostocoleta = pc.id INNER JOIN pagsegurotransacoes pag on pag.id = dpa.pagamento_id INNER JOIN banco b ON b.id = pc.banco_id WHERE dpa.dataliberacaolaudo >= '2021-04-01' and dpa.dataliberacaolaudo <= '2021-04-31' AND pag.TipoPagamento = "Agendamento Pj"  GROUP BY pc.id ORDER BY pc.nome asc


Repasse Empresa

SELECT e.id as EmpresaID, e.nome as EmpresaNome, pc.id as PostoID, pc.nome as PostoNome, epc.repasse FROM `empresaPostocoleta` epc INNER JOIN empresa e on e.id = epc.empresa_id inner join postocoleta pc on pc.id = epc.postocoleta_id order by pc.status desc


=============================================================================================================================================================================================================================================
Faturados sem CNH e MTPS

SELECT pc.nome, dpa.datacoleta, dpa.dataliberacaolaudo, dpa.codigo, dp.nome, dp.cpf, c.nome as Concurso, pag.TipoPagamento FROM dadospessoaisAmostra dpa INNER JOIN postocoleta pc ON pc.id = dpa.idpostocoleta INNER JOIN dadospessoais dp on dp.id = dpa.iddadospessoais INNER JOIN concurso c ON c.id = dpa.idconcurso inner join pagsegurotransacoes pag on pag.id = dpa.pagamento_id where dpa.dataliberacaolaudo >= '2021-05-01' AND dpa.dataliberacaolaudo <= '2021-05-31' and pag.TipoPagamento = "Faturado" and c.id not in (18,13,12,11,5,1143,49,48,47,74,94,156,157,158,159,169,206,269,272,54,55,76)
