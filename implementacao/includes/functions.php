<?php 
	require_once("./includes/conecta.php");

	function verificaPedido($conexao, $email){
		$query = "SELECT * FROM abmcj WHERE email = '{$email}'";
		$db = mysqli_query($conexao, $query);
		$resultado = mysqli_fetch_assoc($db);

		if($resultado){
			if($resultado['conclusao']){				
				reenviaCarteirinha($resultado['nome'], $resultado['email'], $resultado['conclusao']);
?>
			<script type="text/javascript">
				alert("Olá, a confecção da sua carteirinha foi concluída, você receberá o reenvio por e-mail.");
				window.location.href = "/abmcj";
			</script>
<?php								
			}else{							
?>
			<script type="text/javascript">
				alert("Olá, você possui um pedido em andamento conosco, assim que for finalizado, enviaremos a versão digital por e-mail.");
				window.location.href = "/abmcj";
			</script>
<?php				
			}
		}else{
?>
		<script type="text/javascript">			
			window.location.href = "/abmcj/solicitacao.php?email=<?=$email?>";
		</script>
<?php			
		}
?>
		
<?php		
	}

	function reenviaCarteirinha($nome, $emailSolicitante, $conclusao){
		$emailDefinido = "gabrielabreualves@gmail.com";

    	$assunto = "Reenvio de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>Olá ".$nome." recebemos com sucesso o seu pedido de reenvio da carteirinha da ABMCJ: </h3>    		
			<h4>Acesse através deste link: <a href='https://www.victorbraga.com.br/abmcj/$conclusao' target='_blank'><span style='color:#05943e'>Clique Aqui</span></a></h4>						
			<h4>Em caso de dúvida, retorne esse e-mail ou entre em contato via WhatsApp clicando no número <a href='https://api.whatsapp.com/send?phone=5521982845445&text=Olá, possuo dúvidas sobre a minha carteirinha, meu e-mail cadastrado é o $emailDestinatario' target='_blank'>(21) 98284-5445</a>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: ".$nome." <".$emailSolicitante.">";
		$headers[] = "From: Reenvio Carteirinha ABMCJ <".$emailDefinido.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));			
	}

	function enviaEmail($nome, $emailDestinatario, $data, $cargo, $carreira, $comissao, $regional, $inscricao, $comprovante){	
		$emailDefinido = "gabrielabreualves@gmail.com";
	
    	$assunto = "Pedido de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>O Usuário ".$nome." solicitou uma carteirinha da ABMCJ com os seguintes dados: </h3>
    		<h4>Nome: <span style='color:#05943e'>".$nome."</span></h4>     			    		
    		<h4>E-mail: <span style='color:#05943e'>".$emailDestinatario."</span></h4>    			
    		<h4>Data de Admissão: <span style='color:#05943e'>".$data."</span></h4>     			
    		<h4>Cargo: <span style='color:#05943e'>".$cargo."</span></h4>     			
    		<h4>Carreira: <span style='color:#05943e'>".$carreira."</span></h4>				
    		<h4>Comissão: <span style='color:#05943e'>".$comissao."</span></h4>  				
			<h4>Regional: <span style='color:#05943e'>".$regional."</span></h4> 				
			<h4>Inscricao: <span style='color:#05943e'>".$inscricao."</span></h4>			
			<h4>Localização Comprovante: <a href='https://www.victorbraga.com.br/abmcj/$comprovante' target='_blank'><span style='color:#05943e'>".$comprovante."</span></h4>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: Victor Braga <".$emailDefinido.">";
		$headers[] = "From: Solicitação Carteirinha ABMCJ <".$emailDestinatario.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));	
	}


	function enviaEmailSolicitante($nome, $emailSolicitante, $data, $cargo, $carreira, $comissao, $regional, $inscricao){		
		$emailDefinido = "gabrielabreualves@gmail.com";


    	$assunto = "Pedido de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>Olá ".$nome." recebemos com sucesso o seu pedido de carteirinha da ABMCJ com os seguintes dados: </h3>
    		<h4>Nome: <span style='color:#05943e'>".$nome."</span></h4>     			    		
    		<h4>E-mail: <span style='color:#05943e'>".$emailSolicitante."</span></h4>    			
    		<h4>Data de Admissão: <span style='color:#05943e'>".$data."</span></h4>     			
    		<h4>Cargo: <span style='color:#05943e'>".$cargo."</span></h4>     			
    		<h4>Carreira: <span style='color:#05943e'>".$carreira."</span></h4>				
    		<h4>Comissão: <span style='color:#05943e'>".$comissao."</span></h4>  				
			<h4>Regional: <span style='color:#05943e'>".$regional."</span></h4> 				
			<h4>Inscricao: <span style='color:#05943e'>".$inscricao."</span></h4>			
			<h4>Em breve você receberá o acesso ao arquivo Digital.</h4>
			<h4>Em caso de dúvida, retorne esse e-mail ou entre em contato via WhatsApp clicando no número <a href='https://api.whatsapp.com/send?phone=5521982845445&text=Olá, desejo informações sobre o status da minha carteirinha, meu e-mail cadastrado é o $emailDestinatario' target='_blank'>(21) 98284-5445</a>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: ".$nome." <".$emailSolicitante.">";
		$headers[] = "From: Solicitação Carteirinha ABMCJ <".$emailDefinido.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));	
	}
 ?>