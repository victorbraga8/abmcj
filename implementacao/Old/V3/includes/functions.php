<?php 
	require_once("./includes/conecta.php");

	function verificaPedido($conexao, $email){
		echo "Email verifica pedido: ".$email;
		$query = "SELECT * FROM abmcj WHERE email = '{$email}'";
		$db = mysqli_query($conexao, $query);
		$resultado = mysqli_fetch_assoc($db);
		// die();
		if($resultado){
			if($resultado['conclusao']){
				$emailRemetente = "contato@victorbraga.com.br";
?>
			<script type="text/javascript">
				alert("Olá, a confecção da sua carteirinha foi concluída, você receberá o reenvio por e-mail.");
			</script>
<?php				
				reenviaCarteirinha($resultado['nome'], $emailRemetente, $resultado['email'], $resultado['conclusao']);
			}else{							
?>
			<script type="text/javascript">
				alert("Olá, você possui um pedido em andamento conosco, assim que for finalizado, enviaremos a versão digital por e-mail.");
			</script>
<?php				
			}
		}
?>
		
<?php		
	}

	function reenviaCarteirinha($nome, $emailRemetente, $emailSolicitante, $conclusao){
    	$assunto = "Reenvio de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>Olá ".$nome." recebemos com sucesso o seu pedido de reenvio da carteirinha da ABMCJ: </h3>    		
			<h4>Acesse através deste link: <a href='https://www.victorbraga.com.br/abmcj/$conclusao' target='_blank'><span style='color:#05943e'>Clique Aqui</span></a></h4>						
			<h4>Em caso de dúvida, retorne esse e-mail ou entre em contato via WhatsApp clicando no número <a href='https://api.whatsapp.com/send?phone=5521982845445&text=Olá, possuo dúvidas sobre a minha carteirinha, meu e-mail cadastrado é o $emailDestinatario' target='_blank'>(21) 98284-5445</a>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: ".$nome." <".$emailSolicitante.">";
		$headers[] = "From: Solicitação Carteirinha ABMCJ <".$emailRemetente.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));			
	}

	function enviaEmail($nome, $emailSolicitante, $emailDestinatario, $data, $cargo, $carreira, $comissao, $regional, $inscricao, $comprovante){		
    	$assunto = "Pedido de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>O Usuário ".$nome." solicitou uma carteirinha da ABMCJ com os seguintes dados: </h3>
    		<h4>Nome: <span style='color:#05943e'>".$nome."</span></h4>     			    		
    		<h4>E-mail: <span style='color:#05943e'>".$emailSolicitante."</span></h4>    			
    		<h4>Data de Admissão: <span style='color:#05943e'>".$data."</span></h4>     			
    		<h4>Cargo: <span style='color:#05943e'>".$cargo."</span></h4>     			
    		<h4>Carreira: <span style='color:#05943e'>".$carreira."</span></h4>				
    		<h4>Comissão: <span style='color:#05943e'>".$comissao."</span></h4>  				
			<h4>Regional: <span style='color:#05943e'>".$regional."</span></h4> 				
			<h4>Inscricao: <span style='color:#05943e'>".$inscricao."</span></h4>			
			<h4>Localização Comprovante: <a href='https://www.victorbraga.com.br/abmcj/$comprovante' target='_blank'><span style='color:#05943e'>".$comprovante."</span></h4>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: Victor Braga <".$emailDestinatario.">";
		$headers[] = "From: Solicitação Carteirinha ABMCJ <".$emailSolicitante.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));	
	}


	function enviaEmailSolicitante($nome, $emailSolicitante, $emailDestinatario, $data, $cargo, $carreira, $comissao, $regional, $inscricao){		
    	$assunto = "Pedido de Carteirinha: ".$nome;
    	
    	$mensagem = "<html><body><h3>Olá ".$nome." recebemos com sucesso o seu pedido de carteirinha da ABMCJ com os seguintes dados: </h3>
    		<h4>Nome: <span style='color:#05943e'>".$nome."</span></h4>     			    		
    		<h4>E-mail: <span style='color:#05943e'>".$emailSolicitante."</span></h4>    			
    		<h4>Data de Admissão: <span style='color:#05943e'>".$data."</span></h4>     			
    		<h4>Cargo: <span style='color:#05943e'>".$cargo."</span></h4>     			
    		<h4>Carreira: <span style='color:#05943e'>".$carreira."</span></h4>				
    		<h4>Comissão: <span style='color:#05943e'>".$comissao."</span></h4>  				
			<h4>Regional: <span style='color:#05943e'>".$regional."</span></h4> 				
			<h4>Inscricao: <span style='color:#05943e'>".$inscricao."</span></h4>			
			<h4>Em breve você receberá o acesso ao arquivo Digital.</h4>
			<h4>Em caso de dúvida, retorne esse e-mail ou entre em contato via WhatsApp clicando no número <a href='https://api.whatsapp.com/send?phone=5521982845445&text=Olá, desejo informações sobre o status da minha carteirinha, meu e-mail cadastrado é o $emailDestinatario' target='_blank'>(21) 98284-5445</a>
			</body></html>";

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = "To: ".$nome." <".$emailSolicitante.">";
		$headers[] = "From: Solicitação Carteirinha ABMCJ <".$emailRemetente.">";
    	mail($emailRemetente, $assunto, $mensagem, implode("\r\n", $headers));	
	}
 ?>