<?php
    require_once('./includes/head.php');    
?>
  <body>
      <div class = "container" style="margin-top:95px;"> 
        <form method = "POST" enctype = "multipart/form-data" action = "envia.php">
          <div class = "row">
            <div class = "col-12 text-center mb-5 mt-4">
              <h1 class = "display-4"> Solicitação ABMCJ </h1>
            </div>
          </div>
          <div class="row">
          <div class = "form-group mb-4 col-md-6">
            <div class="alert alert-info d-flex align-items-center" role="alert">
              <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
              <div class = "divalert">
                Chave do Pix: <br><strong>abcdeadwad@gmail.com</strong>
              </div>
            </div>
          </div>
          <div class = "form-group mb-4 col-md-6">
            <div class="alert alert-info d-flex align-items-center" role="alert">
              <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
              <div class = "divalert">
              Repasse Bancário: <br><strong>Banco:</strong> Itau - <strong>Agência: </strong>5649 - <strong>Conta Corrente: </strong> 26537-7
              </div>
            </div>
          </div>        
          </div>
          <div class="form-row align-items-center mb-2">
            <div class="form-group col-md-6">
              <label for="inputName" class = "form-label">Nome Completo</label>
              <input type="text" class="form-control" id = "inputName" name = "nome" placeholder="Informe o seu nome" required>
            </div>

            <div class="form-group col-md-6">
              <label for="inputEmail" class = "form-label">Email</label>
              <input type="email" class="form-control" id="inputEmail" name = "email" placeholder = "Digite seu email" required>
            </div>
          </div>

          <div class="form-row align-items-center mb-2">


            <div class="form-group col-md-3">
              <label for="inputData" class = "form-label">Data de Admissão</label>
              <input type="date" class="form-control" id = "inputData" name = "data" required>
            </div>
            <div class="form-group col-md-3">
              <label for="inputCargo" class = "form-label">Cargo</label>
              <input type="text" class="form-control" id = "inputCargo" name = "cargo" placeholder = "Informe seu cargo" required>
            </div>
            <div class="form-group col-md-3">
              <label for="inputCarreira" class = "form-label">Carreira</label>
              <input type="text" class="form-control" id = "inputCarreira" name = "carreira" placeholder = "Informe sua profissão" required>
            </div>

            <div class = "form-group col-md-3">
              <label for = "inputComissao" class = "form-label">Comissão</label>
              <input type = "number" class = "form-control" id = "inputComissao" name = "comissao" placeholder = "Informe sua comissão" required>
            </div>

            <div class = "form-group col-md-3">
              <label for = "inputComissao" class = "form-label">Regional</label>
              <select id="inputRegional" name="regional" class="form-control">
                <option value="#">Selecione a Regional</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantis</option>
              </select>
              <!-- <input type = "number" class = "form-control" id = "inputRegional" name = "regional" placeholder = "Informe sua Regional" required> -->
            </div>            

            <div class = "form-group col-md-3">
              <label for = "inputInscricao" class = "form-label">Inscrição</label>
              <input type = "number" class = "form-control" id = "inputInscricao" name = "inscricao" placeholder = "Informe seu número de inscrição" required>
            </div>
          </div>

          <div class = "form-group mb-3">
            <div class = "mb-2">
              Comprovante de Pagamento
            </div>

            <div class="custom-file col-md-3 mb-3">
              <input type="file" class="custom-file-input" id="customFile" name = "comprovante" required>
              <label class="custom-file-label" for="customFile">Escolher arquivo</label>
            </div>
          </div>

          <div class = "form-group mb-4">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
              <label class="form-check-label" for="flexCheckDefault">
                Confirme o envio do formulário.
              </label>
            </div>
          </div>

          <div class = "form-group"> 
            <button type = "submit" id="btnEnviar" class = "btn btn-danger">Enviar</button>
          </div>  

        </form>

      </div>
      <script src="./includes/scripts.js" ></script>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>      
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

  </body>

</html>
